class Persona{
    constructor(nombre,edad,fecha){
        this.nombre=nombre,
        this.edad=edad,
        this.fecha=fecha
    }
}

class Interface{
    listaPersonal(objPersona){
        let contenedorPersona=document.getElementById("listadoUsuario");
        let contenedor=document.createElement("div");
        contenedor.innerHTML=`
        <div class="py-3">
            <div class="card text-center">
                <div class="card-body">
                <strong>Nombre: </strong> ${objPersona.nombre}
                <strong>Edad: </strong> ${objPersona.edad}
                <strong>Fecha: </strong> ${objPersona.fecha}
                </div>
            </div>
        </div>
        `;
        contenedorPersona.appendChild(contenedor);

    }


    limpiarFormulario(){
        document.getElementById("NuevoPersonal").reset();
    }

}

// DOM DEL NAVEGADOR
// document.getElementById("NuevoPersonal").addEventListener('submit',function(){
//     console.log("ESO ES TODO");
// })

// document.getElementById("NuevoPersonal").addEventListener('submit',function(e){
//     e.preventDefault();
//     console.log("ESO ES TODO");
// })

document.getElementById("NuevoPersonal").addEventListener('submit',function(e){
    e.preventDefault();
    let nombre=document.getElementById("nombre").value;
    let edad=document.getElementById("edad").value;
    let fecha=document.getElementById("fechaRegistro").value;

    let newPersona=new Persona(nombre,edad,fecha);
    let interfacePersona=new Interface();

    interfacePersona.listaPersonal(newPersona);
    interfacePersona.limpiarFormulario();


})

